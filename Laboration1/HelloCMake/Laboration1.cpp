// HelloCMake.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;
int main() {
	//Skapar variabler
	float Startkm, Stopkm, Result, Milesdriven, Refuel1, Fuelconsump, Cost, Resultcost;

	cout << "BENSINF\x94RBRUKNING OCH KOSTNAD" << endl;
	cout << "=============================" << endl;
	//Ange mätarställning 1 och sparar värdet i en variabel.
	cout << "Ange m\x84tarst\x84llning 1 [km]: ";
	cin >> Startkm;

	//Ange mätarställning 2 och sparar värdet i en variabel
	cout << "Ange m\x84tarst\x84llning 2 [km]: ";
	cin >> Stopkm;
	//Sparar värdet av mätarställning 2 minus mätarställning 1 variabeln "Result"
	Result = Stopkm - Startkm;
	//Sparar värdet av "Result" delat i 10 i variabeln "Milesdriven".
	Milesdriven = Result / 10;
	//Skriver ut antal mil sedan senaste tankningen.
	cout << "Du har k\x94rt " << Milesdriven << "Mil sedan senaste tankningen" << endl;

	//Ange antal liter tankat och sparar värdet i en variabel
	cout << "Ange hur mycket du tankade [L]: ";
	cin >> Refuel1;
	//Sätter värdet av Fuelconsump till antal liter tankat / antal mil körda
	Fuelconsump = Refuel1 / Milesdriven;
	//Skriver ut bensinförbrukning med max två decimaler.
	cout.precision(2);
	cout << fixed << "Bensinf\x94rbrukning [L/Mil]: " << Fuelconsump << endl;
	//Ange bensinpris och sparar värdet i en variabel
	cout << "Ange bensinpris [Kr/L]: ";
	cin >> Cost;
	//Sätter värdet av Resultcost till bensinpriset * bensinförbrukning
	Resultcost = Cost * Fuelconsump;
	//Skriver ut bensinkostnad med max två decimaler.
	cout.precision(2);
	cout << fixed << "Bensinkostnad [Kr/Mil]: " << Resultcost << endl;

}

